# To init project you must to run this commands in main project folder:

* composer update
* php bin/console d:mig:m
* php bin/console doctrine:fixtures:load
* php bin/console asset:install --symlink
* php bin/console server:run

## Site info:

* user: admin
* pass: admin

