<?php

namespace AdminBundle\DataFixtures\ORM;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class User extends Fixture
{
    public function load(ObjectManager $em)
    {
        $userObj = new \AdminBundle\Entity\User();
        $userStr = "a:8:{i:0;s:60:\"$2y$13$4nXW70IQNMq1SK3qQA7MiOsvGfA7HVzqagQq0SYdxI0hjTqDYgUjC\";i:1;N;i:2;s:5:\"admin\";i:3;s:5:\"admin\";i:4;b:1;i:5;i:1;i:6;s:15:\"admin@gmail.com\";i:7;s:15:\"admin@gmail.com\";} ";
        $userObj->unserialize($userStr);
        $em->persist($userObj);
        $em->flush();
    }
}
