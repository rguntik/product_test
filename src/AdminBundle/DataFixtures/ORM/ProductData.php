<?php

namespace AdminBundle\DataFixtures\ORM;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use AdminBundle\Entity\Category;
use AdminBundle\Entity\Product;

class ProductData extends Fixture
{
    public function load(ObjectManager $em)
    {
        $categories = [];
        for ($i = 1; $i < 11; $i++) {
            $category = new Category();
            $categories[] = $category->setName("Category {$i}");
            $em->persist($category);
            $em->flush();
        }

        $productCount = 50;
        for ($i = 1; $i < $productCount; $i++) {
            $product = new Product();
            $product->setName("Product {$i}");
            $categoryId = rand(0, 8);
            $product->addCategory($categories[$categoryId]);
            $product->addCategory($categories[$categoryId + 1]);
            $em->persist($product);
            $em->flush();
        }
    }
}
