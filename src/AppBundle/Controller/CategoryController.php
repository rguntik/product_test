<?php

namespace AppBundle\Controller;

use AdminBundle\Entity\Category;
use AdminBundle\Entity\Product;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

class CategoryController extends Controller
{
    /**
     * @Route(
     *     "/category/{category}",
     *     defaults={"category": null},
     *     name="product_category"
     *     )
     */
    public function indexAction(Request $request, Category $category = null)
    {
        $em = $this->getDoctrine()->getManager();

        return $this->render('category/index.html.twig', [
            'category' => $category,
            'categories' => $em->getRepository(Category::class)->findAll(),
        ]);
    }
}
